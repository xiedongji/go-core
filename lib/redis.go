package lib

import (
	"blog/core/conf"
	"blog/core/log"
	"errors"
	"fmt"
	"github.com/garyburd/redigo/redis"
	"math/rand"
	"time"
)

/* =================================================================
	redis 初始化操作
 * =================================================================*/
func RedisConnFactory(name string) (redis.Conn, error) {
	if conf.IConfRedisMap != nil && conf.IConfRedisMap.List != nil {
		// confName 配置文件名称/配置文件
		for confName, cfg := range conf.IConfRedisMap.List {
			//加载 指定的配置文件
			if name == confName {
				//随机负载
				randHost := cfg.ProxyList[rand.Intn(len(cfg.ProxyList))]
				//如果配置文件没有设置该值 则 设置默认值
				if cfg.ConnTimeout == 0 {
					cfg.ConnTimeout = 50
				}
				if cfg.ReadTimeout == 0 {
					cfg.ReadTimeout = 100
				}
				if cfg.WriteTimeout == 0 {
					cfg.WriteTimeout = 100
				}
				conn, err := redis.Dial(
					"tcp",
					randHost,
					redis.DialConnectTimeout(time.Duration(cfg.ConnTimeout)*time.Millisecond),
					redis.DialReadTimeout(time.Duration(cfg.ReadTimeout)*time.Millisecond),
					redis.DialWriteTimeout(time.Duration(cfg.WriteTimeout)*time.Millisecond))
				if err != nil {
					return nil, err
				}
				if cfg.Password != "" {
					if _, err := conn.Do("AUTH", cfg.Password); err != nil {
						conn.Close()
						return nil, err
					}
				}
				if cfg.Db != 0 {
					if _, err := conn.Do("SELECT", cfg.Db); err != nil {
						conn.Close()
						return nil, err
					}
				}
				return conn, nil
			}
		}
	}
	return nil, errors.New("create redis conn fail")
}

//通过配置 执行redis
func RedisConfDo(name string, commandName string, args ...interface{}) (interface{}, error) {
	//调用上面的 数据库初始化 操作
	c, err := RedisConnFactory(name)
	if err != nil {
		tipsMaps := map[string]interface{}{
			"method": commandName,
			"err":    errors.New("RedisConnFactory_error:" + name),
			"bind":   args,
		}

		log.ErrorMap("com_redis_failure:", tipsMaps)
		return nil, err
	}
	defer c.Close()

	startExecTime := time.Now()
	reply, err := c.Do(commandName, args...)
	endExecTime := time.Now()
	if err != nil {
		tipsMaps := map[string]interface{}{
			"method":    commandName,
			"err":       err,
			"bind":      args,
			"proc_time": fmt.Sprintf("%fs", endExecTime.Sub(startExecTime).Seconds()),
		}
		log.ErrorMap("com_redis_failure:", tipsMaps)
	} else {
		replyStr, _ := redis.String(reply, nil)
		tipsMaps := map[string]interface{}{
			"method":    commandName,
			"bind":      args,
			"reply":     replyStr,
			"proc_time": fmt.Sprintf("%fs", endExecTime.Sub(startExecTime).Seconds()),
		}
		log.INFOMap("com_redis_success", tipsMaps)
	}
	return reply, err
}
