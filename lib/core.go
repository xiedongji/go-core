package lib

import (
	"blog/core/conf"
	"flag"
	"fmt"
	"log"
	"os"
	"time"
)

/* =================================================================
	模块初始化 配置
	eg: core.InitCore("./conf/dev/",[]string{"base", "mysql", "redis"})
 * =================================================================*/
func InitCore(configDirPath string, modules []string) error {
	var configDir *string
	if len(configDirPath) > 0 {
		configDir = &configDirPath
	} else {
		//命令行提示
		configDir = flag.String("config", "", "Please input config file. ex ./conf/dev/")
		flag.Parse()
	}

	//如果没有传入配置文件 系统退出
	if *configDir == "" {
		flag.Usage()
		os.Exit(1)
	}

	log.Println("---------------------------------------------------------")
	log.Printf("[INFO] load config path = %s\n", *configDir)
	log.Printf("[INFO] %s\n", "start loading resources")

	//初始化配置文件目录
	if err := conf.InitConfEnv(*configDir); err != nil {
		return err
	}

	//加载 base 配置文件 到实体对象
	if IsStrInArray("base", modules) {
		if err := conf.InitBaseConf("base"); err != nil {
			fmt.Printf("[ERROR] %s %s\n", time.Now().Format(TimeFormat), "InitBaseConf:"+err.Error())
		}
	}

	//加载 redis 配置文件 到实体对象
	if IsStrInArray("redis", modules) {
		if err := conf.InitRedisConf("redis_cfg"); err != nil {
			fmt.Printf("[ERROR] %s %s\n", time.Now().Format(TimeFormat), " InitRedisConf:"+err.Error())
		}
	}

	// 加载 mysql 配置文件 到实体对象
	if IsStrInArray("mysql", modules) {
		if err := InitDbPool("mysql_cfg"); err != nil {
			fmt.Printf("[ERROR] %s%s\n", time.Now().Format(TimeFormat), " MySQL连接报错!!!InitDBPool:"+err.Error())
		}
	}

	log.Printf("[INFO] %s\n", "success loading resources")
	log.Println("---------------------------------------------------------")
	return nil
}

/* =================================================================
	整个APP执行结束 回调的函数
	主要用于 关闭 数据连接 日志文件流关闭
 * =================================================================*/
func Destroy() {
	log.Println("----------------------------------------------------------")
	log.Printf("[INFO] %s\n", " start destroy resources")
	//关闭数据库连接池
	CloseDB()
	log.Printf("[INFO] %s\n", " success destroy resources")
}
