package lib

import (
	"crypto/md5"
	"encoding/hex"
	"net"
)

/* =================================================================
	获取本地IP地址 除去回环地址
 * =================================================================*/
func GetLocalIPs() (ips []net.IP) {
	interfaceAddr, err := net.InterfaceAddrs()
	if err != nil {
		return nil
	}
	for _, address := range interfaceAddr {
		ipNet, isValidIpNet := address.(*net.IPNet)
		if isValidIpNet && !ipNet.IP.IsLoopback() {
			if ipNet.IP.To4() != nil {
				ips = append(ips, ipNet.IP)
			}
		}
	}
	return ips
}

/* =================================================================
	判断字符串 是否在 字符数组中
 * =================================================================*/
func IsStrInArray(target string, arr []string) bool {
	for _, item := range arr {
		if item == target {
			return true
		}
	}
	return false
}

/* =================================================================
	Substr 字符串的截取
 * =================================================================*/
func Substr(str string, start int64, end int64) string {
	length := int64(len(str))
	if start < 0 || start > length {
		return ""
	}
	if end < 0 {
		return ""
	}
	if end > length {
		end = length
	}
	return string(str[start:end])
}

/* =================================================================
	MD5加密
 * =================================================================*/
func GenMd5Str(text string) string {
	calc := md5.New()
	calc.Write([]byte(text))
	return hex.EncodeToString(calc.Sum(nil))
}

//带返回报错
func GenMd5Err(data string) (string, error) {
	h := md5.New()
	_, err := h.Write([]byte(data))
	if err != nil {
		return "", err
	}
	return hex.EncodeToString(h.Sum(nil)), nil
}
