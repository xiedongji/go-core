package conf

import (
	"strings"
)

var (
	IConfEnvPath string //配置文件目录路径
	IConfEnv     string //配置环境名 dev prod
)

/* ============================================================================
	====初始化 配置文件环境===
	解析配置文件目录
	eg：config=conf/dev/base.toml 	ConfEnvPath=conf/dev	ConfEnv=dev
 * ============================================================================*/
func InitConfEnv(configDir string) error {
	path := strings.Split(configDir, "/")
	IConfEnv = path[len(path)-2]
	IConfEnvPath = strings.Join(path[:len(path)-1], "/")

	//配置文件 转 Viper
	err := convertTomlToViper()
	if err != nil {
		return err
	}

	return nil
}

//获取配置环境名称
func GetConfEnv() string {
	return IConfEnv
}
