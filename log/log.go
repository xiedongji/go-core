package log

import "log"

func Error(tips string, info interface{})  {
	log.Printf("[ERROR] %s: %+v\n", tips, info)
}

func ErrorMap(tips string, infoMap map[string]interface{})  {
	log.Printf("[ERROR] %s: %+v\n", tips, mapToString(infoMap))
}

func INFO(tips string, info interface{})  {
	log.Printf("[INFO] %s: %+v\n", tips, info)
}

func INFOMap(tips string, infoMap map[string]interface{})  {
	log.Printf("[INFO] %s: %+v\n", tips, mapToString(infoMap))
}

func mapToString(infoMap map[string]interface{}) string {
	tmpStr := ""
	for tag, val := range infoMap {
		if v, ok := val.(string); ok {
			tmpStr = tmpStr + "|" +tag+":"+v
		}
	}
	return tmpStr
}
